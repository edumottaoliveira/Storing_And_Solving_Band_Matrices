#ifndef BAND_MATRIX_AL_SYSTEM_H
#define BAND_MATRIX_AL_SYSTEM_H

#include "bandMatrixAL.h"
#include "vector.h"

typedef struct {
	BandMatrixAL* matrixA;
	Vector* vectorB;
	Vector* vectorSolution;
} BandMatrixALSystem;

BandMatrixALSystem* createBandMatrixALSystem(BandMatrixAL* matrixA, Vector* vectorB);

void freeBandMatrixALSystem(BandMatrixALSystem *ls);

void printBandMatrixALSystem(BandMatrixALSystem *ls);

void performGausReductionBandMatrixAL(BandMatrixALSystem* linearSystem);

void performBackSubstitutionBandMatrixAL(BandMatrixALSystem* linearSystem);

void solveBandMatrixALSystem(BandMatrixALSystem* linearSystem);

#endif
