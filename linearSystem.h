#ifndef LINEARSYSTEM_H
#define LINEARSYSTEM_H

#include "matrix.h"
#include "vector.h"

typedef struct {
	Matrix* matrixA;
	Vector* vectorB;
	Vector* vectorSolution;
} LinearSystem;

LinearSystem* createLinearSystem(Matrix* matrixA, Vector* vectorB);

void freeLinearSystem(LinearSystem *ls);

void printLinearSystem(LinearSystem *ls);

void performGausRowReduction(LinearSystem* linearSystem);

void performBackSubstitution(LinearSystem* linearSystem);

void solveLinearSystem(LinearSystem* linearSystem);

#endif
