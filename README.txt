Passos para a execução do códigoÇ


1. Executar o Makefile: Altere a localização para o diretório raiz e compile os arquivos usando o comando "make" no terminalç

2. Entrar com o comando: "./exComp1 -xx y", onde:

	-xx: pode ser "-CR" ou "-AL". Adicionando CR os testes serão executados em uma matriz de banda com o primeiro método de armazenamento definido neste exercício, a compressão por colunas (Compressed Row). AL executa os testes com as mesmas matrizes num formato de Array de Listas.

	y: Um valor de 1 a 4 representando um sistema a ser testado.

	Exemplos: 
	> ./exComp1 -AL 3
	> ./exComp1 -CR 1
	> ./exComp1 -AL 4

