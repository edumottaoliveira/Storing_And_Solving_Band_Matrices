/*
 ============================================================================
 Nome      : exercicioComp1.c
 Autor     : Eduardo Motta de Oliveira
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "matrix.h"
#include "vector.h"
#include "linearSystem.h"
#include "bandMatrixCR.h"
#include "bandMatrixCRSystem.h"
#include "bandMatrixAL.h"
#include "bandMatrixALSystem.h"
#include "util.h"

void teste0();
void testeCR_1();
void testeCR_2();
void testeCR_3();
void testeCR_4();
void testeAL_1();
void testeAL_2();
void testeAL_3();
void testeAL_4();

int main(int argc, char *argv[]) {

	if (argc < 3) {
		printf("Comando incorreto!\n");
		printf("Exemplo de uso: \"exComp1 -CR 1\" \n");
		return EXIT_SUCCESS;
	}

	if (strcmp(argv[1], "-CR") == 0) {
		int testNum = atoi(argv[2]);
		switch(testNum) {
		case 1:
			testeCR_1();
			break;
		case 2:
			testeCR_2();
			break;
		case 3:
			testeCR_3();
			break;
		case 4:
			testeCR_4();
			break;
		default:
			printf("Nao ha teste com esse numero.\n");
		}
	}
	else if (strcmp(argv[1], "-AL") == 0) {
		int testNum = atoi(argv[2]);
		switch(testNum) {
		case 1:
			testeAL_1();
			break;
		case 2:
			testeAL_2();
			break;
		case 3:
			testeAL_3();
			break;
		case 4:
			testeAL_4();
			break;
		default:
			printf("Nao ha teste com esse numero.\n");
		}
	} else {
		printf("Entrada incorreta.\n");
	}
	return EXIT_SUCCESS;
}


void teste0() {
	/*
	A:
		3.00	2.00	1.00
		1.00	3.00	1.00
		2.00	2.00	3.00

	b:
		6.00
		5.00
		7.00

	Sol:
		1.00
		1.00
		1.00
	*/
	printf("=> Teste 0\n");
	Matrix* A0 = allocateMatrix(3, 3);

	A0->cells[0][0] = 3;
	A0->cells[0][1] = 2;
	A0->cells[0][2] = 1;

	A0->cells[1][0] = 1;
	A0->cells[1][1] = 3;
	A0->cells[1][2] = 1;

	A0->cells[2][0] = 2;
	A0->cells[2][1] = 2;
	A0->cells[2][2] = 3;

	Vector* b0 = allocateVector(3);

	b0->cells[0] = 6;
	b0->cells[1] = 5;
	b0->cells[2] = 7;

	LinearSystem* ls = createLinearSystem(A0,b0);
	printLinearSystem(ls);
	solveLinearSystem(ls);
	printLinearSystem(ls);
	freeLinearSystem(ls);
}

void testeCR_1() {
	/*
	A:
		2.00	3.00	0.00	0.00	0.00
		1.00	4.00	2.00	0.00	0.00
		4.00	11.00	5.00	2.00	0.00
		0.00	15.00	3.00	6.00	3.00
		0.00	0.00	2.50	24.00	8.00

	b:
		1.00
		1.00
		1.00
		1.00
		1.00

	Sol:
		0.96
		-0.31
		0.64
		-1.32
		3.88
	*/

	printf("=> Teste Matrix CR 1\n");

	BandMatrixCR* A = allocateBandMatrixCR(5,4,-2,5);
	A->cells[0][0] = 0;
	A->cells[0][1] = 0;
	A->cells[0][2] = 2;
	A->cells[0][3] = 3;

	A->cells[1][0] = 0;
	A->cells[1][1] = 1;
	A->cells[1][2] = 4;
	A->cells[1][3] = 2;

	A->cells[2][0] = 4;
	A->cells[2][1] = 11;
	A->cells[2][2] = 5;
	A->cells[2][3] = 2;

	A->cells[3][0] = 15;
	A->cells[3][1] = 3;
	A->cells[3][2] = 6;
	A->cells[3][3] = 3;

	A->cells[4][0] = 2.5;
	A->cells[4][1] = 24;
	A->cells[4][2] = 8;
	A->cells[4][3] = 0;

	Vector* b = allocateVector(5);
	b->cells[0] = 1;
	b->cells[1] = 1;
	b->cells[2] = 1;
	b->cells[3] = 1;
	b->cells[4] = 1;

	BandMatrixCRSystem* bs = createBandMatrixCRSystem(A,b);
	printBandMatrixCRSystem(bs);
	solveBandMatrixCRSystem(bs);
	printBandMatrixCRSystem(bs);
	freeBandMatrixCRSystem(bs);
}

void testeAL_1() {
	printf("=> Teste Matrix AL 1\n");
	/*
	A:
		2.00	3.00	0.00	0.00	0.00
		1.00	4.00	2.00	0.00	0.00
		4.00	11.00	5.00	2.00	0.00
		0.00	15.00	3.00	6.00	3.00
		0.00	0.00	2.50	24.00	8.00

	b:
		1.00
		1.00
		1.00
		1.00
		1.00

	Sol:
		0.96
		-0.31
		0.64
		-1.32
		3.88
	*/

	BandMatrixAL* a = allocateBandMatrixAL(5, 5, -2);

	insertNewCellBandMatrixAL(a, 0, 0, 2);
	insertNewCellBandMatrixAL(a, 0, 1, 3);

	insertNewCellBandMatrixAL(a, 1, 0, 1);
	insertNewCellBandMatrixAL(a, 1, 1, 4);
	insertNewCellBandMatrixAL(a, 1, 2, 2);

	insertNewCellBandMatrixAL(a, 2, 0, 4);
	insertNewCellBandMatrixAL(a, 2, 1, 11);
	insertNewCellBandMatrixAL(a, 2, 2, 5);
	insertNewCellBandMatrixAL(a, 2, 3, 2);

	insertNewCellBandMatrixAL(a, 3, 1, 15);
	insertNewCellBandMatrixAL(a, 3, 2, 3);
	insertNewCellBandMatrixAL(a, 3, 3, 6);
	insertNewCellBandMatrixAL(a, 3, 4, 3);

	insertNewCellBandMatrixAL(a, 4, 2, 2.5);
	insertNewCellBandMatrixAL(a, 4, 3, 24);
	insertNewCellBandMatrixAL(a, 4, 4, 8);

	Vector* b = allocateVector(5);
	b->cells[0] = 1;
	b->cells[1] = 1;
	b->cells[2] = 1;
	b->cells[3] = 1;
	b->cells[4] = 1;

	BandMatrixALSystem* ls = createBandMatrixALSystem(a,b);
	printBandMatrixALSystem(ls);
	solveBandMatrixALSystem(ls);
	printBandMatrixALSystem(ls);
	freeBandMatrixALSystem(ls);
}

void testeCR_2() {
	printf("=> Teste Matrix CR 2\n");
	/*
	A:
		2.00	3.00	0.00	0.00	0.00
		1.00	4.00	2.00	0.00	0.00
		4.00	11.00	5.00	2.00	0.00
		0.00	15.00	3.00	6.00	3.00
		0.00	0.00	2.50	24.00	8.00
	b:
		1.00
		1.00
		1.00
		1.00
		1.00
	Sol:
		0.96
		-0.31
		0.64
		-1.32
		3.88
	*/

	BandMatrixCR* A = allocateBandMatrixCR(5,4,-2,5);
	A->cells[0][0] = 0;
	A->cells[0][1] = 0;
	A->cells[0][2] = 2;
	A->cells[0][3] = 3;

	A->cells[1][0] = 0;
	A->cells[1][1] = 1;
	A->cells[1][2] = 4;
	A->cells[1][3] = 2;

	A->cells[2][0] = 4;
	A->cells[2][1] = 11;
	A->cells[2][2] = 5;
	A->cells[2][3] = 2;

	A->cells[3][0] = 15;
	A->cells[3][1] = 3;
	A->cells[3][2] = 6;
	A->cells[3][3] = 3;

	A->cells[4][0] = 2.5;
	A->cells[4][1] = 24;
	A->cells[4][2] = 8;
	A->cells[4][3] = 0;

	Vector* b = allocateVector(5);
	b->cells[0] = 1;
	b->cells[1] = 1;
	b->cells[2] = 1;
	b->cells[3] = 1;
	b->cells[4] = 1;

	BandMatrixCRSystem* bs = createBandMatrixCRSystem(A,b);
	printBandMatrixCRSystem(bs);
	solveBandMatrixCRSystem(bs);
	printBandMatrixCRSystem(bs);
	freeBandMatrixCRSystem(bs);
}

void testeAL_2() {
	printf("=> Teste Matrix AL 2\n");
	/*
	A:
		2.00	3.00	0.00	0.00	0.00
		1.00	4.00	2.00	0.00	0.00
		4.00	11.00	5.00	2.00	0.00
		0.00	15.00	3.00	6.00	3.00
		0.00	0.00	2.50	24.00	8.00
	b:
		1.00
		1.00
		1.00
		1.00
		1.00
	Sol:
		0.96
		-0.31
		0.64
		-1.32
		3.88
	*/

	BandMatrixAL* a = allocateBandMatrixAL(5, 5, -2);
	insertNewCellBandMatrixAL(a, 0, 0, 2);
	insertNewCellBandMatrixAL(a, 0, 1, 3);

	insertNewCellBandMatrixAL(a, 1, 0, 1);
	insertNewCellBandMatrixAL(a, 1, 1, 4);
	insertNewCellBandMatrixAL(a, 1, 2, 2);

	insertNewCellBandMatrixAL(a, 2, 0, 4);
	insertNewCellBandMatrixAL(a, 2, 1, 11);
	insertNewCellBandMatrixAL(a, 2, 2, 5);
	insertNewCellBandMatrixAL(a, 2, 3, 2);

	insertNewCellBandMatrixAL(a, 3, 1, 15);
	insertNewCellBandMatrixAL(a, 3, 2, 3);
	insertNewCellBandMatrixAL(a, 3, 3, 6);
	insertNewCellBandMatrixAL(a, 3, 4, 3);

	insertNewCellBandMatrixAL(a, 4, 2, 2.5);
	insertNewCellBandMatrixAL(a, 4, 3, 24);
	insertNewCellBandMatrixAL(a, 4, 4, 8);

	Vector* b = allocateVector(5);
	b->cells[0] = 1;
	b->cells[1] = 1;
	b->cells[2] = 1;
	b->cells[3] = 1;
	b->cells[4] = 1;

	BandMatrixALSystem* ls = createBandMatrixALSystem(a,b);
	printBandMatrixALSystem(ls);
	solveBandMatrixALSystem(ls);
	printBandMatrixALSystem(ls);
	freeBandMatrixALSystem(ls);
}

void testeCR_3() {
	printf("=> Teste Matrix CR 3\n");
	/*
	a = [
	8, 3, 1, 0, 0, 0, 0;
	6, 1, 5, 8, 0, 0, 0;
	0, 7, 3, 1, 2, 0, 0;
	0, 0, 5, 6, 4, 1, 0;
	0, 0, 0, 1, 3, 1, 2;
	0, 0, 0, 0, 2, 5, 4;
	0, 0, 0, 0, 0, 2, 1; ]

	b = [1; 2; 3; 1; 4; 2; 6;]

	x =
		0.32212
		5.14005
		-16.99709
		9.98909
		4.01091
		10.00728
		-14.01455
	*/

	BandMatrixCR* A = allocateBandMatrixCR(7,4,-1,7);

	A->cells[0][0] = 0;
	A->cells[0][1] = 8;
	A->cells[0][2] = 3;
	A->cells[0][3] = 1;

	A->cells[1][0] = 6;
	A->cells[1][1] = 1;
	A->cells[1][2] = 5;
	A->cells[1][3] = 8;

	A->cells[2][0] = 7;
	A->cells[2][1] = 3;
	A->cells[2][2] = 1;
	A->cells[2][3] = 2;

	A->cells[3][0] = 5;
	A->cells[3][1] = 6;
	A->cells[3][2] = 4;
	A->cells[3][3] = 1;

	A->cells[4][0] = 1;
	A->cells[4][1] = 3;
	A->cells[4][2] = 1;
	A->cells[4][3] = 2;

	A->cells[5][0] = 2;
	A->cells[5][1] = 5;
	A->cells[5][2] = 4;
	A->cells[5][3] = 0;

	A->cells[6][0] = 2;
	A->cells[6][1] = 1;
	A->cells[6][2] = 0;
	A->cells[6][3] = 0;

	Vector* b = allocateVector(7);
	b->cells[0] = 1;
	b->cells[1] = 1;
	b->cells[2] = 1;
	b->cells[3] = 1;
	b->cells[4] = 1;
	b->cells[5] = 1;
	b->cells[6] = 1;

	BandMatrixCRSystem* bs = createBandMatrixCRSystem(A,b);
	printBandMatrixCRSystem(bs);
	solveBandMatrixCRSystem(bs);
	printBandMatrixCRSystem(bs);
	freeBandMatrixCRSystem(bs);
}

void testeAL_3() {
	printf("=> Teste Matrix AL 3\n");
	/*
	a = [
		8, 3, 1, 0, 0, 0, 0;
		6, 1, 5, 8, 0, 0, 0;
		0, 7, 3, 1, 2, 0, 0;
		0, 0, 5, 6, 4, 1, 0;
		0, 0, 0, 1, 3, 1, 2;
		0, 0, 0, 0, 2, 5, 4;
		0, 0, 0, 0, 0, 2, 1; ]

	x =
		0.32212
		5.14005
		-16.99709
		9.98909
		4.01091
		10.00728
		-14.01455
	*/

	BandMatrixAL* a = allocateBandMatrixAL(7, 7, -1);

	insertNewCellBandMatrixAL(a, 0, 0, 8);
	insertNewCellBandMatrixAL(a, 0, 1, 3);
	insertNewCellBandMatrixAL(a, 0, 2, 1);

	insertNewCellBandMatrixAL(a, 1, 0, 6);
	insertNewCellBandMatrixAL(a, 1, 1, 1);
	insertNewCellBandMatrixAL(a, 1, 2, 5);
	insertNewCellBandMatrixAL(a, 1, 3, 8);

	insertNewCellBandMatrixAL(a, 2, 1, 7);
	insertNewCellBandMatrixAL(a, 2, 2, 3);
	insertNewCellBandMatrixAL(a, 2, 3, 1);
	insertNewCellBandMatrixAL(a, 2, 4, 2);

	insertNewCellBandMatrixAL(a, 3, 2, 5);
	insertNewCellBandMatrixAL(a, 3, 3, 6);
	insertNewCellBandMatrixAL(a, 3, 4, 4);
	insertNewCellBandMatrixAL(a, 3, 5, 1);

	insertNewCellBandMatrixAL(a, 4, 3, 1);
	insertNewCellBandMatrixAL(a, 4, 4, 3);
	insertNewCellBandMatrixAL(a, 4, 5, 1);
	insertNewCellBandMatrixAL(a, 4, 6, 2);

	insertNewCellBandMatrixAL(a, 5, 4, 2);
	insertNewCellBandMatrixAL(a, 5, 5, 5);
	insertNewCellBandMatrixAL(a, 5, 6, 4);

	insertNewCellBandMatrixAL(a, 6, 5, 2);
	insertNewCellBandMatrixAL(a, 6, 6, 1);

	Vector* b = allocateVector(7);
	b->cells[0] = 1;
	b->cells[1] = 1;
	b->cells[2] = 1;
	b->cells[3] = 1;
	b->cells[4] = 1;
	b->cells[5] = 1;
	b->cells[6] = 1;

	BandMatrixALSystem* ls = createBandMatrixALSystem(a,b);
	printBandMatrixALSystem(ls);
	solveBandMatrixALSystem(ls);
	printBandMatrixALSystem(ls);
	freeBandMatrixALSystem(ls);
}

void testeCR_4() {
	printf("=> Teste Matrix CR 4\n");
	/*
	A:
		 3.00	 6.00	 0.00	 0.00
		 0.00	 2.00	 1.00	 0.00
		 0.00	 0.00	 8.00	 3.00
		 0.00	 0.00	 0.00	 9.00
	b:
		2.00
		5.00
		6.00
		3.00
	Sol:
		-3.71
		2.19
		0.62
		0.33
	*/
	BandMatrixCR* A = allocateBandMatrixCR(4,2,0,4);
	A->cells[0][0] = 3;
	A->cells[0][1] = 6;
	A->cells[1][0] = 2;
	A->cells[1][1] = 1;
	A->cells[2][0] = 8;
	A->cells[2][1] = 3;
	A->cells[3][0] = 9;
	A->cells[3][1] = 0;

	Vector* b = allocateVector(4);
	b->cells[0] = 2;
	b->cells[1] = 5;
	b->cells[2] = 6;
	b->cells[3] = 3;

	BandMatrixCRSystem* bs = createBandMatrixCRSystem(A,b);
	printBandMatrixCRSystem(bs);
	solveBandMatrixCRSystem(bs);
	printBandMatrixCRSystem(bs);
	freeBandMatrixCRSystem(bs);
}

void testeAL_4() {
	printf("=> Teste Matrix AL 4\n");
	/*
	A:
		 3.00	 6.00	 0.00	 0.00
		 0.00	 2.00	 1.00	 0.00
		 0.00	 0.00	 8.00	 3.00
		 0.00	 0.00	 0.00	 9.00
	b:
		2.00
		5.00
		6.00
		3.00
	Sol:
		-3.71
		2.19
		0.62
		0.33
	*/
	BandMatrixAL* a = allocateBandMatrixAL(4, 4, 0);
	insertNewCellBandMatrixAL(a, 0, 0, 3);
	insertNewCellBandMatrixAL(a, 0, 1, 6);
	insertNewCellBandMatrixAL(a, 1, 1, 2);
	insertNewCellBandMatrixAL(a, 1, 2, 1);
	insertNewCellBandMatrixAL(a, 2, 2, 8);
	insertNewCellBandMatrixAL(a, 2, 3, 3);
	insertNewCellBandMatrixAL(a, 3, 3, 9);

	Vector* b = allocateVector(4);
	b->cells[0] = 2;
	b->cells[1] = 5;
	b->cells[2] = 6;
	b->cells[3] = 3;

	BandMatrixALSystem* ls = createBandMatrixALSystem(a,b);
	printBandMatrixALSystem(ls);
	solveBandMatrixALSystem(ls);
	printBandMatrixALSystem(ls);
	freeBandMatrixALSystem(ls);
}


