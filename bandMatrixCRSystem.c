#include "bandMatrixCRSystem.h"
#include <stdio.h>
#include "util.h"

BandMatrixCRSystem* createBandMatrixCRSystem(BandMatrixCR* matrixA, Vector* vectorB) {
	BandMatrixCRSystem* ls = (BandMatrixCRSystem*) malloc (sizeof(BandMatrixCRSystem));
	ls->matrixA = matrixA;
	ls->vectorB= vectorB;
	Vector* sol = allocateVector(matrixA->numberOfColumnsExpanded);
	ls->vectorSolution = sol;
	return ls;
}

void freeBandMatrixCRSystem(BandMatrixCRSystem *ls) {
	if (ls == NULL) {
		printf("null");
		return;
	}
	freeBandMatrixCR(ls->matrixA);
	freeVector(ls->vectorB);
	freeVector(ls->vectorSolution);
	FREE(ls);
}

void printBandMatrixCRSystem(BandMatrixCRSystem *ls) {
	printBandMatrixCR(ls->matrixA,"A");
	printVector(ls->vectorB,"b");
	printVector(ls->vectorSolution,"Sol");
}

void solveBandMatrixCRSystem(BandMatrixCRSystem* linearSystem) {
		if (linearSystem == NULL)
			return;
		printf("\nResolvendo sistema linear...\n");
		performGausReductionBandMatrixCR(linearSystem);
		performBackSubstitutionBandMatrixCR(linearSystem);
}

void performGausReductionBandMatrixCR(BandMatrixCRSystem* linearSystem) {
	double** a = linearSystem->matrixA->cells;
	double* b = linearSystem->vectorB->cells;
	int numRows = linearSystem->matrixA->numberOfRows;
	int numCols = linearSystem->matrixA->numberOfColumns;
	int offset = linearSystem->matrixA->offsetFirstRow;
	double m, pivot;
	int pivotColumn = (-1) * offset;

	for (int pivotRow = 0; pivotRow < numRows-1; pivotRow++) {
		pivot = a[pivotRow][pivotColumn];
		if (pivot == 0.0)
			ERROR("Divisao por zero em performGausRowReduction.");

		int offsetIteratedRow = 0;
		for (int i = pivotRow+1; i <= pivotRow-offset && i < numRows; i++) {
			offsetIteratedRow -= 1;
			m = a[i][pivotColumn + offsetIteratedRow]/pivot;
			a[i][pivotColumn + offsetIteratedRow] = 0.0;

			for (int jPivot = pivotColumn + 1; jPivot < numCols; jPivot++) {
				a[i][jPivot+offsetIteratedRow] -= m*a[pivotRow][jPivot];
			}
			b[i] = b[i] - m*b[pivotRow];
		}
	}

}

void performBackSubstitutionBandMatrixCR(BandMatrixCRSystem* linearSystem) {

	double** matrixA = linearSystem->matrixA->cells;
	double* vectorB = linearSystem->vectorB->cells;
	double* vectorSolution = linearSystem->vectorSolution->cells;
	int numRows = linearSystem->matrixA->numberOfRows;
	int numCols = linearSystem->matrixA->numberOfColumns;
	int expantedNumColumns = linearSystem->matrixA->numberOfColumnsExpanded;
	int offsetFirstRow = linearSystem->matrixA->offsetFirstRow;


	for (int i = numRows - 1; i >= 0; i--) {
		double sum = 0.0;
		int offsetIteratedRow = offsetFirstRow + i;

		for (int j = i+1 ; j < i+numCols+offsetFirstRow  && j < expantedNumColumns; j++) {
			sum += matrixA[i][j - offsetIteratedRow] * vectorSolution[j];
		}

		vectorSolution[i] = (vectorB[i] - sum)/ matrixA[i][i - offsetIteratedRow];
	}
}
