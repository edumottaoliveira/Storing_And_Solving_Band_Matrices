#include "linearSystem.h"
#include "util.h"

LinearSystem* createLinearSystem(Matrix* matrixA, Vector* vectorB) {
	if (matrixA->numberOfColumns != vectorB->numberOfElements)
		return NULL;

	LinearSystem* ls = (LinearSystem*) malloc (sizeof(LinearSystem));
	ls->matrixA = matrixA;
	ls->vectorB = vectorB;
	Vector* sol = allocateVector(vectorB->numberOfElements);
	ls->vectorSolution = sol;
	return ls;
}

void freeLinearSystem(LinearSystem *ls) {
	freeMatrix(ls->matrixA);
	freeVector(ls->vectorB);
	freeVector(ls->vectorSolution);
	FREE(ls);
}

void printLinearSystem(LinearSystem *ls) {
	printMatrix(ls->matrixA,"A");
	printVector(ls->vectorB,"b");
	printVector(ls->vectorSolution,"Sol");
}

void performGausRowReduction(LinearSystem* linearSystem) {

	double** a = linearSystem->matrixA->cells;
	double* b = linearSystem->vectorB->cells;
	int numRows = linearSystem->matrixA->numberOfRows;
	int numCols = linearSystem->matrixA->numberOfColumns;
	double m, pivot;

	for (int k = 0; k < linearSystem->matrixA->numberOfColumns; k++) {
		pivot = a[k][k];
		if (pivot == 0.0)
			ERROR("Divisao por zero em performGausRowReduction.");

		for (int i = k+1; i < numRows; i++) {
			m = a[i][k]/pivot;

			for (int j = 0; j < numCols; j++) {
				a[i][j] = a[i][j] - m*a[k][j];
			}
			b[i] = b[i] - m*b[k];
		}
	}
}

void performBackSubstitution(LinearSystem* linearSystem) {
	int numRows = linearSystem->matrixA->numberOfRows;
	int numCols = linearSystem->matrixA->numberOfColumns;
	double** matrixA = linearSystem->matrixA->cells;
	double* vectorB = linearSystem->vectorB->cells;
	double* vectorSolution = linearSystem->vectorSolution->cells;
	int k = numCols - 1;

	for (int i = numRows - 1; i >= 0; i--) {
		double sum = 0.0;
		for (int j = k; j < numCols; j++) {
			sum += matrixA[i][j] * vectorSolution[j];
		}
		vectorSolution[i] = (vectorB[i] - sum)/ matrixA[i][k]; // Tratar div por zero
		k--;
	}
}

void solveLinearSystem(LinearSystem* linearSystem) {
		if (linearSystem == NULL)
			return;
		printf("\nResolvendo sistema linear...\n");
		performGausRowReduction(linearSystem);
		performBackSubstitution(linearSystem);
}


