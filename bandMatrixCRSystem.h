#ifndef BAND_MATRIX_CR_SYSTEM_H
#define BAND_MATRIX_CR_SYSTEM_H

#include "bandMatrixCR.h"
#include "vector.h"

typedef struct {
	BandMatrixCR* matrixA;
	Vector* vectorB;
	Vector* vectorSolution;
} BandMatrixCRSystem;

BandMatrixCRSystem* createBandMatrixCRSystem(BandMatrixCR* matrixA, Vector* vectorB);

void freeBandMatrixCRSystem(BandMatrixCRSystem *ls);

void printBandMatrixCRSystem(BandMatrixCRSystem *ls);

void performGausReductionBandMatrixCR(BandMatrixCRSystem* linearSystem);

void performBackSubstitutionBandMatrixCR(BandMatrixCRSystem* linearSystem);

void solveBandMatrixCRSystem(BandMatrixCRSystem* linearSystem);

#endif
