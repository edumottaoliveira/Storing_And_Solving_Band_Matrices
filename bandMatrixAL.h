#ifndef BAND_MATRIX_AL_H
#define BAND_MATRIX_AL_H

typedef struct matrixCell{
	unsigned int column;
	double value;
	struct matrixCell* next;
} MatrixCell;

/* Banded Matrix - Array of Lists */
typedef struct {
	MatrixCell** cells;
	int numberOfRows;
	int numberOfColumnsExpanded;
	int offsetFirstRow;
} BandMatrixAL;

BandMatrixAL* allocateBandMatrixAL(int numberOfRows, int numberOfColumnsExpanded, int offsetFirstRow);

MatrixCell* allocateMatrixCell(int column, double value);

void insertNewCellBandMatrixAL(BandMatrixAL* matrix, int row, int column, double value);

void freeBandMatrixAL(BandMatrixAL* matrix);

void freeMatrixCell(MatrixCell* c);

double getElemBandMatrixAL(BandMatrixAL* m, int row, int column);

MatrixCell* getCellBandMatrixAL(BandMatrixAL* m, int row, int column);

void printBandMatrixAL(BandMatrixAL* matrix, char* description);

#endif
