#include "vector.h"
#include "util.h"

Vector* allocateVector(int numberOfElements) {
	Vector* v = (Vector*) malloc(sizeof(Vector));
	v->cells = (double*) calloc(numberOfElements, sizeof(double));
	v->numberOfElements = numberOfElements;
	return v;
}

void freeVector(Vector* vector) {
	FREE(vector->cells);
	FREE(vector);
}

void printVector(Vector* vector, char* description) {
	printf("\n%s:\n", description);
	for (int j = 0; j < vector->numberOfElements; j++) {
		printf("\t%.2lf\n", vector->cells[j]);
	}
}
