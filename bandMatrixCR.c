#include <stdio.h>
#include "bandMatrixCR.h"
#include "util.h"

BandMatrixCR* allocateBandMatrixCR(int numberOfRows, int numberOfColumns, int offsetFirstCollum, int numberOfColumnsExpanded) {
	BandMatrixCR* m = (BandMatrixCR*) malloc(sizeof(BandMatrixCR));
	m->cells = (double**) malloc(numberOfRows * sizeof(double**));
	for (int i = 0; i < numberOfRows; i++) {
		m->cells[i] = (double*) calloc(numberOfColumns, sizeof(double));
	}
	m->numberOfRows = numberOfRows;
	m->numberOfColumns = numberOfColumns;
	m->offsetFirstRow = offsetFirstCollum;
	m->numberOfColumnsExpanded = numberOfColumnsExpanded;
	return m;
}


void freeBandMatrixCR(BandMatrixCR* matrix) {
	if(matrix == NULL)
		return;
	for (int i = 0; i < matrix->numberOfRows; i++) {
		FREE(matrix->cells[i]);
	}
	FREE(matrix->cells);
	FREE(matrix);
}

void printBandMatrixCR(BandMatrixCR* matrix, char* description) {
	int offsetFirstRow = matrix->offsetFirstRow;
	int numCols = matrix->numberOfColumns;
	int numRows = matrix->numberOfRows;
	int expantedNumColumns = matrix->numberOfColumnsExpanded;

	printf("\n%s:\n", description);
	for (int i = 0; i < numRows; i++) {
		int fistNonZeroColumn = i + offsetFirstRow;
		int lastNonZeroColumn = i + offsetFirstRow + numCols - 1;

		for (int j = 0; j < expantedNumColumns; j++) {
			double cellValue = 0.0;
			if (j >= fistNonZeroColumn && j <= lastNonZeroColumn){
				cellValue = matrix->cells[i][j - fistNonZeroColumn];
			}
			printf("\t%.2lf", cellValue);
		}
		printf("\n");
	}
}
