#include <stdio.h>
#include "bandMatrixAL.h"
#include "util.h"

BandMatrixAL* allocateBandMatrixAL(int numberOfRows, int numberOfColumnsExpanded, int offsetFirstRow){
	BandMatrixAL* m = (BandMatrixAL*) malloc(sizeof(BandMatrixAL));
	m->cells = (MatrixCell**) malloc(numberOfRows * sizeof(MatrixCell**));
	for (int i = 0; i < numberOfRows; i++) {
		m->cells[i] = NULL;
	}

	m->numberOfRows = numberOfRows;
	m->numberOfColumnsExpanded = numberOfColumnsExpanded;
	m->offsetFirstRow = offsetFirstRow;
	return m;
}

MatrixCell* allocateMatrixCell(int column, double value) {
	MatrixCell* c = (MatrixCell*) malloc (sizeof (MatrixCell));
	c->column = column;
	c->value = value;
	c->next = NULL;
	return c;
}

void insertNewCellBandMatrixAL(BandMatrixAL* matrix, int row, int column, double value){
	if (matrix->cells[row] == NULL) {
		matrix->cells[row] = allocateMatrixCell(column, value);
		return;
	}

	MatrixCell* current = matrix->cells[row];
	MatrixCell* next = current->next;
	while (next != NULL) {
		current = next;
		next = current->next;
	}
	current->next = allocateMatrixCell(column, value);
}

void freeBandMatrixAL(BandMatrixAL* matrix) {
	if(matrix == NULL)
		return;
	for (int i = 0; i < matrix->numberOfRows; i++) {
		MatrixCell* current = matrix->cells[i];
		MatrixCell* next = matrix->cells[i]->next;
		while (next != NULL) {
			current = next;
			next = next->next;
			freeMatrixCell(current);
		}
		freeMatrixCell(matrix->cells[i]);
	}
	FREE(matrix->cells);
	FREE(matrix);
}



void freeMatrixCell(MatrixCell* c) {
	c->next = NULL;
	FREE(c);
}


double getElemBandMatrixAL(BandMatrixAL* m, int row, int column) {

	MatrixCell* cell = m->cells[row];

	while (cell != NULL) {
		if (cell->column == column) {
			return cell->value;
		}
		else if (cell->column < column){
			cell = cell->next;
		}
		else {
			break;
		}
	}
	return 0.0;
}

MatrixCell* getCellBandMatrixAL(BandMatrixAL* m, int row, int column) {
	MatrixCell* cell = m->cells[row];

	while (cell != NULL) {

		if (cell->column == column) {
			return cell;
		}
		else if (cell->column < column){
			cell = cell->next;
		}
		else {
			return NULL;
		}
	}
	return NULL;
}

void printBandMatrixAL(BandMatrixAL* matrix, char* description) {
	printf("\n%s:\n", description);
	for (int i = 0; i < matrix->numberOfRows; i++) {
		for (int j = 0; j < matrix->numberOfColumnsExpanded; j++) {
			double value = getElemBandMatrixAL(matrix, i, j);
			printf("\t %.2lf",value);
		}
		printf("\n");
	}
}
