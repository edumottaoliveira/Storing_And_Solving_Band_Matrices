#include "matrix.h"
#include "util.h"

Matrix* allocateMatrix(int numberOfRows, int numberOfColumns) {
	Matrix* m = (Matrix*) malloc(sizeof(Matrix));
	m->cells = (double**) malloc(numberOfRows * sizeof(double**));
	for (int i = 0; i < numberOfRows; i++) {
		m->cells[i] = (double*) calloc(numberOfColumns, sizeof(double));
	}
	m->numberOfRows = numberOfRows;
	m->numberOfColumns = numberOfColumns;
	return m;
}

void freeMatrix(Matrix* matrix) {
	for (int i = 0; i < matrix->numberOfRows; i++) {
		FREE(matrix->cells[i]);
	}
	FREE(matrix->cells);
	FREE(matrix);
}

void printMatrix(Matrix* matrix, char* description) {
	printf("\n%s:\n", description);
	for (int i = 0; i < matrix->numberOfRows; i++) {
		for (int j = 0; j < matrix->numberOfColumns; j++) {
			printf("\t%.2lf", matrix->cells[i][j]);
		}
		printf("\n");
	}
}

/*
Matrix* loadMatrixFromFile(char* fileName) {
	 FILE* file = fopen(filename, "r");
	 if (file) {
	     while ((c = getc(file)) != EOF)
	         putchar(c);
	     fclose(file);
	 }
}*/
