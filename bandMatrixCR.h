#ifndef BAND_MATRIX_CR_H
#define BAND_MATRIX_CR_H

/* Banded Matrix - Compressed Row */
typedef struct {
	double** cells;
	int numberOfRows;
	int numberOfColumns;
	int offsetFirstRow;
	int numberOfColumnsExpanded;
} BandMatrixCR;

BandMatrixCR* allocateBandMatrixCR(int numberOfRows, int numberOfColumns, int offsetFirstCollum, int numberOfColumnsExpanded);

void freeBandMatrixCR(BandMatrixCR* matrix);

void printBandMatrixCR(BandMatrixCR* matrix, char* description);

#endif
