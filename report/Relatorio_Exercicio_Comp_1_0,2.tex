%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Journal Article
% LaTeX Template
% Version 1.3 (9/9/13)
%
% This template has been downloaded from:
% http://www.LaTeXTemplates.com
%
% Original author:
% Frits Wenneker (http://www.howtotex.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------------------------------------------------------------------------------
%       PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------
\documentclass[paper=letter, fontsize=12pt]{article}
\usepackage[brazil]{babel} % English language/hyphenation
\usepackage{amsmath,amsfonts,amsthm} % Math packages
\usepackage[utf8]{inputenc}
\usepackage{float}
\usepackage{lipsum} % Package to generate dummy text throughout this template
\usepackage{blindtext}
\usepackage{graphicx} 
\usepackage{caption}
\usepackage{subcaption}
\usepackage[sc]{mathpazo} % Use the Palatino font
%\usepackage{fourier} % Use the Adobe Utopia font for the document - comment this line to return to the LaTeX default
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\linespread{1.05} % Line spacing - Palatino needs more space between lines
\usepackage{microtype} % Slightly tweak font spacing for aesthetics
\usepackage[hmarginratio=1:1,top=32mm,columnsep=20pt]{geometry} % Document margins
\usepackage{multicol} % Used for the two-column layout of the document
%\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} % Custom captions under/above floats in tables or figures
\usepackage{booktabs} % Horizontal rules in tables
\usepackage{float} % Required for tables and figures in the multi-column environment - they need to be placed in specific locations with the [H] (e.g. \begin{table}[H])
\usepackage{hyperref} % For hyperlinks in the PDF
\usepackage{lettrine} % The lettrine is the first enlarged letter at the beginning of the text
\usepackage{paralist} % Used for the compactitem environment which makes bullet points with less space between them
\usepackage{abstract} % Allows abstract customization
\renewcommand{\abstractnamefont}{\normalfont\bfseries} % Set the "Abstract" text to bold
\renewcommand{\abstracttextfont}{\normalfont\small\itshape} % Set the abstract itself to small italic text
\usepackage{titlesec} % Allows customization of titles

\renewcommand\thesection{\Roman{section}} % Roman numerals for the sections
\renewcommand\thesubsection{\Roman{subsection}} % Roman numerals for subsections

\titleformat{\section}[block]{\large\scshape\centering}{\thesection.}{1em}{} % Change the look of the section titles
\titleformat{\subsection}[block]{\large}{\thesubsection.}{1em}{} % Change the look of the section titles
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal rule command with 1 argument of height
\usepackage{fancyhdr} % Headers and footers
\pagestyle{fancy} % All pages have headers and footers
\fancyhead{} % Blank out the default header
\fancyfoot{} % Blank out the default footer

\fancyhead[C]{Universidade Federal do Espírito Santo $\bullet$ 16 de Abril de 2017} % Custom header text

\fancyfoot[RO,LE]{\thepage} % Custom footer text

\usepackage{listings} 

\usepackage{tikz}
\usetikzlibrary{positioning}
\tikzset{
node of list/.style = { 
             draw, 
             fill=gray!20, 
             minimum height=6mm, 
             minimum width=6mm,
             node distance=6mm
   },
link/.style = {
     -stealth,
     shorten >=1pt
     },
array element/.style = {
    draw, fill=white,
    minimum width = 6mm,
    minimum height = 10mm
  }
}

\def\LinkedList#1{%
  \foreach \element in \list {
     \node[node of list, right = of aux, name=\element] {\element};
     \draw[link] (aux) -- (\element);
     \coordinate (aux) at (\element.east);
  } 
}
%----------------------------------------------------------------------------------------
%       TITLE SECTION
%----------------------------------------------------------------------------------------
\title{\vspace{-15mm}\fontsize{24pt}{10pt}\selectfont\textbf{Resolução de Sistemas Lineares com Matrizes Esparsas de Banda}} % Article title
\author{
\large
{\textsc{Eduardo Motta de Oliveira }}\\[2mm]
%\thanks{A thank you or further information}\\ % Your name
%\normalsize \href{mailto:marco.torres.810@gmail.com}{marco.torres.810@gmail.com}\\[2mm] % Your email address
}
\date{}

%----------------------------------------------------------------------------------------
\begin{document}
\maketitle % Insert title
\thispagestyle{fancy} % All pages have headers and footers

Este relatório foi desenvolvido para o primeiro exercício computacional do curso de Algorithmos Numéricos II. O exercício consiste em implementar dois modelos de armazenamento de matrizes de banda e resolve-los de forma otimizada usando os métodos diretos de resolução de sistemas lineares. O objetivo deste documento é mostrar o que foi implementado no código em anexo, explicar o motivo das decisões tomadas para armazenar e solucionar os sistemas com matrizes esparsas de banda e comparar os resultados para identificar a melhor solução ou os melhores métodos adotados em termos de perfomance computacional.

\section{Introdução}

Matrizes de banda são uma categoria especial de matriz esparsa, onde os elementos não nulos distriuem-se numa faixa (ou uma banda) que cobre e envolve a diagonal principal. Por possuir elementos aglutinados numa área específica da matriz, as matrizes de banda podem fazer uso de estratégias mais otimizadas para o armazenamento dos seus elementos e resolução de listemas lineares.

O presente exercício define duas formas de armazenar esse tipo de matriz, que receberam a seguinte denominação:

\begin{itemize}
\item \textit{Matriz de Banda com Array de Listas}: Armazena listas das células não nulas por linha da matriz de forma indexada em um array principal.

\item \textit{Matriz de Banda com Compressão por Colunas}: Armazena uma matriz tradicional contendo as linhas não nulas da matriz de banda. Algumas células com valor nulo podem ser adicionadas para manter a distribuição dos índicies padronizada.
\end{itemize}

Os métodos de armazenamento serão exemplificados e melhor descritos nas duas seções seguintes. As últimas seções deste relatório serão dedicadas à análize dos metodos de rezolução usando os dois modelos de armazenamento.

\section{Matriz de Banda com Array de Listas}

O formato mais otimizado de armazenamento de uma matriz esparsa deveria eliminar toda célula nula para evitar o desperdício de memória. Como a quantidade de células nulas é variável nas linhas e colunas, o uso de listas encadiadas pode ser uma solução flexível. 

Porém, para a indexação dos elementos por linha e coluna novos valores terão de ser armazenados nas estruturas, reduzindo a eficiência do armazenamento. Desta forma, optou-se aqui por uma solução mista: linhas são armazenadas em listas encadeadas e, cada uma dessas listas é armazenada de maneira ordenada em um array, com acesso direto aos seus campos.

Um exemplo da transformação de uma matriz tradicional de banda em  uma na forma de array de listas é mostrado a seguir. As células nulas, com valor igual a zero, foram substítuidas por pontos ($.$) na ilustração para facilitar a visualização. 

\[
   M=
  \left[ {\begin{array}{ccccc}
   a & b & . & . & . \\
   c & d & e & . & . \\
   . & f & g & h & . \\
   . & . & i & j & k \\
   . & . & . & l & m \\
  \end{array} } \right]
\]


\begin{center}

\begin{tikzpicture}
$M' =$ \hspace{0.5cm}
\foreach \index/\list in {0/{a,b,null}, 1/{c,d,e,null}, 2/{f,g,h,null}, 3/{i,j,k,null}, 4/{l,m,null}} {
   \node[array element] (aux) at (0,-\index) {\index};
   \LinkedList{\list}
}
\end{tikzpicture}
\end{center}

Como podemos observar, a matriz no último  formato não armazena os coeficientes nulos, eliminando o desperdício desnecessário de espaço de armazenamento. Porém, em virtude da necessidade de localização das células pelo formato de índice $(linha,coluna)$, o array principal é usado como índice das linhas e um atributo em cada célula da lista encadeada armazena o número da coluna. Um exemplo da estrutura de cada célula, escrito em linguagem C é mostrado abaixo:

\begin{lstlisting}[frame=single]  % Start your code-

struct CelulaDaMatriz {
	double valorDaCelula;
	int numeroDaColuna;
	struct CelulaDaMatriz* proximaCelula;
}
\end{lstlisting}

Assim, é possível facilmente acessar os elementos da matriz através de uma função que selecione a linha certa indexada pelo array e percorra a lista encadeada em busca da célula com a coluna correta.

Percorrer uma lista encadeada, contudo, pode ser uma operação custosa. Além disso, a adição de novos atributos também acarreta uma maior necessidade de armazenamento, o que pode resultar em um desperdício de memória maior que o da matriz original. 

Por exemplo, a matriz $M$ utilizava 25 elementos armazenados no formato $double$ da linguagem C, que num sistema de 32bits ocupa um total de $25 * 8 bytes = 200 bytes$. A matriz convertida $M'$ armazena um array com 5 ponteiros de $4bytes$, que por sua vez apontam para estruturas com 13 células encadeadas, cada uma ocupando pelo menos $20bytes$ ($8 + 4 + 8$). O total de memória usado pelo segundo formato ficaria em $340bytes$ ($5 * 4 + 16*20$). Um aumento de 70\% no uso de memória!

O uso da memória, porém, é diretamente proporcial à quantidade de valores não nulos das células, do que podemos concluir que a economia de memória ocorrerá em matrizes maiores, de banda pequena e com muitos valores nulos.

\section{Resolução de Sistemas com Matrizes de Banda com Array de Listas}

Definida no código em anexo como $BandMatrixAL$ a estrutura da Matriz de Banda com Array de Listas guarda, além do array de listas de células as informações seguintes para serem resolvidas num tradicional sistema linear:

\begin{itemize}
\item \textit{Número de linhas}: Um inteiro que define o número de linhas e, conseguentemente, o tamanho do arrayç

\item \textit{Número de colunas na forma expandida}: Um inteiro que define o número original de colunas da matriz, antes de ser convertida;

\item \textit{Offset da primeira linha}: Um inteiro que identifica o deslocamento da banda na primeira linha. Por exemplo, se a banda é de três elementos (três elementos não nulos alinhados horizontalmente) e na primeira linha da matriz só há um elemento não nulo, então consideramos como se os outros dois elementos estivessem deslocados para a esquerda, fora da área útil da matriz. Nesse caso o valor do offset seria de $-2$.
\end{itemize}

No caso das Matrizes de Banda, é interessante observar que --- solução direta -- metodo de gauss -- primeiro e ultimo elemento não alterado.

A resolução do sistema aqui implementada faz uso desses atributos para reduzir o número de iterações necessárias, iterando somente nas células não nulas, nas que não sofrem atualização e nas que não precisam ter seu valor computado.

%iteracoes 
\section{Matriz de Banda com Compressão por Colunas}

O formato de armazenamento Matriz de Banda com Compressão por Colunas remove a maioria das células nulas enquanto justapõe os valores não nulos formando uma matrix com o mesmo número de linhas mas um menor número de colunas. 

Essa transformação pode ser exemplificada abaixo, onde a matriz $M$ é convertida na matriz $M'$ para que seja armazenada com mais eficiência.

\[
   M=
  \left[ {\begin{array}{ccccc}
   a & b & . & . & . \\
   c & d & e & . & . \\
   . & f & g & h & . \\
   . & . & i & j & k \\
   . & . & . & l & m \\
  \end{array} } \right]
\]

\[
   M'=
  \left[ {\begin{array}{ccc}
   . & a & b \\
   c & d & e \\
   f & g & h \\
   i & j & k \\
   l & m & . \\
  \end{array} } \right]
\]

É possível observar nesse exemplo que a matriz $M (5 \times 5)$ foi transformada numa $M' (5 \times 3)$, ou seja, uma redução de 40\% no espaço de armazenamento necessário para representá-la. Essa redução do espaço nessessário irá depender da proporção de valores nulos da matriz original, que por sua vez também pode ser determinado pelo tamanho total da matriz e tamanho da banda.

Denominando $m$ e $n$ o número de linhas e colunas, reepectivamente, e $b$ o tamanho da banda (no exemplo acima era 3), então podemos chegar a conclusão que uma matriz $(m \times n)$ poderá ser armazenada por esse formato numa matriz $m \times b$, e a redução se dará numa proporção de $n - b$. 

A conversão de $M$ em $M'$, apesar de simples, apresenta um problema: como acessar os elementos da maneira correta para resolver um sistema linear onde a matriz pricipal está armazenada nesse formato? A seção 5 irá apresentar como esse processo será feito na resolução de um sistema pelo método de eliminação de Gauss.

\section{Model Development}


In developing the model, we used the same specification as Stock and Watson (1999) \cite{stock}. The main investigation of the paper is to compare different specifications of the Phillips curve, in particular, with varying indicators of aggregate economic activity. The authors compare between three specifications of the model:
\begin{itemize} 
\item The traditional Phillips curve with unemployment rate as explanatory variable
\item An alternative specification with different aggregate economic activities (industrial production, real personal income, capacity utilization rate in manufacturing etc.) 
\item Specification with different macroeconomic variables, such as money supply, stock prices, output, wages, interest rates and exchange rates
\end{itemize}

We take the former two specifications mentioned above, the traditional Phillips Curve and an alternative model with different indicators for economic activity. The first model specified in equation (\ref{eq1}), \textit{lhur} is the traditional Phillips curve with unemployment rate and its lags as variables for indicating aggregate economic activity. Notable feature in this particular model is that the dependent variable is change in inflation rate over periods longer than the sampling frequency (in this case, monthly). It allows predicting inflation change $h$ periods ahead.  
\begin{equation}
\label{eq1}
\pi_{t+h}^{h} - \pi_{t} = \phi + \beta(L) u_{t} + \gamma(L) \triangle \pi_{t} + e_{t+h}
\end{equation}
This specification restricts the model in two ways. \textit{First}, the inflation is I(1) process. Equation (\ref{eq1}) is the same if we leave $\pi_{t+h}^{h}$ on the left hand side and replace  $\gamma(L) \triangle \pi_{t}$ with $\mu(L)\pi_{t}$ on the right hand side, with restriction $\mu(1)=1$ (the first restriction). For $h=12$, this specification can be thought of as predicting inflation over the next twelve months using a distributed lag of current and past inflation, subject to the restriction that lag coefficients sum up to one. \textit{Second}, the non-accelerating inflation rate of unemployment (NAIRU) is constant.  Expressing the constant as $\phi = -\beta(1) \bar{u} $ in Equation (\ref{eq2}), we can see the second restriction imposed on the model. 
\begin{equation}
\label{eq2}
\pi_{t+h}^{h} - \pi_{t} = \phi + \beta(L) (u_{t} - \bar{u}) + \gamma(L) \triangle \pi_{t} + e_{t+h}
\end{equation}
 
The next model we treat, uses alternative macroeconomic indicators $x_{t}$s instead of $u_{t}$ in equation (1). For variable $x_{t}$ we took the six aggregate indicators mentioned above: IP, GMPYQ, MSMTQ, LPNG, IPXMCA and HSBP. 


The IP, GMPYQ, MSMTQ, LPNAG are non-stationary I(1), thus we made transformations using the Hodrick-Prescott (HP) filter. Therefore, all $x_{t}$  are treated as I(0). Specifically, we took the gap estimates from HP filter which is separated from the trend component. The transformation of the variables can be seen in the appendix. We chose the lag lengths of independent variables that minimizes the Schwarz information criterion (BIC), setting the maximum number of lags at 11 as it was proposed in the paper. 

\begin{equation}
\label{eq2}
\pi_{t+h}^{h} - \pi_{t} = \beta(L) x_{t} + \gamma(L) \triangle \pi_{t} + e_{t+h}
\end{equation}
In total, we estimate the model with seven different variables and its first differences for both price indexes, CPI and PCE.  %How many steps forward and out of sample

\section{Model discussion}


The theory about the relation between unemployment and inflation dates back to 1958. Phillips' empirical work showed a relation between unemployment rate and the rate of change on wages in the United Kingdom. Later on, Milton Friedman established a more modern specification for the relationship, introducing the natural rate of unemployment and stating that variations from this rate would impact changes on price inflation only on the short-run. The basic idea is that a higher economic activity (lower unemployment) indicates that the economy is producing closer to its maximum and, therefore, demand might excede the supply capacity, raising prices.  

As explained above, the main goal is to test various alternative specifications of the Phillips curve against the traditional model that uses unemployment. We must point out that each regression is estimated again every month and the forecast of the subsequent month is computed. In order to compare which forecast  performed better, two measures are used. The first one is the \textit{relative mean squared error (RMSE)}. The mean squared errors are calculated as follows: first we find the difference between real values of the inflation rate change and the forecast values. We square these differences and take the mean value of the squared errors. Then the relative mean squared errors are calculated as the ratio of MSE from a model using $x_{t}$ to the benchmark model (using unemployment rate - $u_{t}$). 

If the $RMSE >1$, then, the traditional specification of the model with $u_{t}$ is outperforming the alternative one in terms of efficiency. Or, in other words, unemployment predicts future inflation better than a given variable $x_{t}$. 
By a \text{forecast combining regression} (\ref{eq3}), we can derive the other measure used to compare forecasts performances: 
\begin{equation}
\label{eq3}
\pi_{t+h}^{h} - \pi_{t} = \lambda f_{t}^{x} + (1-\lambda) f_{t}^{u} + e_{t+h}
\end{equation}Equation (\ref{eq3}) is a regression of the actual value of inflation on the two different forecasts $f_{t}^x$ and $f_{t}^u$, with $x_t$ and $u_{t}$ respectively. $\lambda$ indicates how much each forecast estimation add to each other. If the estimated $\hat{\lambda} >1$, the  $f_{t}^x$ is a better forecast, which means that the forecasts based on unemployment rate adds nothing to the forecasts based on other economic activity measures. On the other hand, when $\hat{\lambda} =0$, forecast based on unemployment $f_t^u$ is better than the forecasts based on the $x_t$. We get these measures for every specification of the model and presented them in Table 1 of Appendix. This is the main replication of the model in Stock and Watson (1999). Table 2 in their paper does exactly the same as ours, but using different periods (1970-1996). 

By the results on Table 2, we can see that the traditional Phillips curve is not the best predictor of future inflation for the period from 2000 to 2013. In the gaps specification, the RMSE is bigger than 1 only for housing starts (HSBP). The capacity utilization rate in manufacturing (IPXMCA) is slightly bigger than 1 only for the model using CPI. The $\lambda$ are also substantially larger than 0 and give the same qualitative result as the RMSE. For the first differences specification, the same is valid. Only the number of employees on non-agricultural payrolls (LPNG) under-performs unemployment on forecasting CPI inflation. All the other estimates are better for predicting future inflation.

The comparison with the univariate equation reveals that the Phillips curve is a poor predictor of inflation on the period 2000-2013. The RMSE of the univariate regression is much smaller than the benchmark and $\lambda$ is bigger than 1 for both inflation indexes. Moreover, the RSME of the univariate case is also the lowest among all. It means that past values of inflation alone predicts its future values better than when including covariates related to economic activity. This result is different from the result of Stock and Watson (1999) and it is probably a consequence of the period we used.

Between 2000 and 2013, it seems that unemployment and inflation were barely related. Figure 1 in the appendix shows the series of Unemployment, CPI and PCE from 1970 to 2013. Before 2000, it is possible to observe a negative relationship between the two variables on the short-run. However, after 2000, and especially after 2008 (economical crisis), the relation is almost absent. Figure 2 only focus on the period 2000-2013 and we can see better that, in 2008, there was a great increase on unemployment rate (from around 4\% to 11\%). The inflation rate measured by the CPI has fallen in 2008\footnote{The CPI inflation puts much more weight on real state than the PCE one, which explains the strong fall right after the crisis (when house prices has fallen substantially), differently from the PCE}, but it seems to behave independently of unemployment after the initial shock. For the case of PCE inflation, the drop was small in 2008, and it continued at similar levels even after unemployment started to fall again (unemployment rate was lower than 6\% in 2013). This pattern explains why the RMSE of the univariate model using PCE inflation was so low (0.11). Overall, it shows that the forecast using the Phillips curve, as performed by Stock and Watson (1999), is not successful for predicting the inflation of recent years. Moreover, among those specifications, the one using unemployment was one of the worst for predicting future inflation.

\section{Model evaluation}


In order to evaluate the model presented in the paper, we performed a number of steps. First, we decided to estimate Equation \ref{eq2} in-sample for both CPI and PCE in period 1990:1-2013:12. Then we diagnosed the property of the residuals, namely independence. What we observe is that the residuals from the estimated model are strongly correlated over time, which of course violates the model assumption. The Q-statistics of the Ljung-Box tests as well as both autocorrelation and partial autocorrelation functions are presented in Figures \ref{resug} (for PCE) and \ref{resup} (for CPI) in the Appendix.

Second, using the parameters from the model, we simulated inflation using Monte Carlo procedure with 1000 repetitions. In our simulated data, errors are normally distributed and uncorrelated. The variance of the errors was adjusted using the following formula: $\sigma^2_{sim} = (\sigma_{\pi} - \sigma_{fit})^2$, where $\sigma_{\pi}$ is the standard deviation of the true inflation and $\sigma_{fit}$ is the standard deviation of the fitted values from our model. In the Figures \ref{resugsim} and \ref{resupsim} in the Appendix, there are the PACF and ACF with Ljung-Box test statistics for simulated PCE and CPI respectively. We can clearly see that the simulated model meets the assumption. In addition, for all lags, we fail to reject the null hypothesis that residuals are independently distributed.

Based on our 1000 simulated samples, we estimated exactly the same models as using actual data and made a set of in-sample forecasts for the period 2002:1-2013:12. Next, for each Monte Carlo repetition, we calculated the mean-squared error of the simulated inflation forecast and them compared it to the mean-squared error of the forecasts based on real values of inflation. As a result we obtain 1000 relative MSE for both CPI and PCE. The results of this simulation procedure are presented in the Table \ref{Table2} in the Appendix. 

Due to the fact that in the model based on true data the assumption of dependency was violated, mean-squared errors of the forecasts were much larger than for our simulated data. We can also observe that the relative mean squared errors obtained from real CPI and simulated CPI are almost two times smaller to those obtained from PCE simulation procedure. This might suggest that the problem of errors autocorrelation is much larger for the PCE inflation index.



\section{Conclusions}


Figure 1 plots annual inflation rates, $\pi_{t}^{12}$ for two US monthly price indexes. The pattern of inflation calculated by these two indexes are same as the original paper between 1970 and 2000. We focus the analysis on inflation measures between 2000 and 2013 which are plotted in Figure 2. During this time window, these two measures have different patterns. First, CPI inflation varies more than PCE inflation does. Second, PCE inflation is larger than CPI inflation most of time except in year 2008. The reason, why the CPI is much more volatile than PCE in our sample, is the fact that in case of CPI the weight put on housing prices is substantially larger than in case of PCE (40\% and 22\% respectively). 

The results of inflation forecasts based on measures of aggregate real activity  are shown in Table \ref{Table 1}. We forecast the inflation between 2000 and 2013 rather than divide it into two sub-periods as what the paper does and have the following findings:

First, same as the original paper, we also find that there are important differences in the forecastability of inflation across price measures. PCE inflation forecasts are more accurate than CPI forecasts. For the univariate case, the RMSE for PCE is only one fourth of that for CPI. For the other aggregate real activity factors, only two variables (MSMTQ and HSBP)'s RMSEs are larger than those for CPI. For the first differences specification, the RMSE for PCE is much smaller. 

Second, most of the estimated values of $\lambda$ are significantly greater than $0$. In contrast to the magnitude of $\lambda$ in the original paper, the estimated $\lambda$ in our results are larger and most  of them are larger than 1 and some even larger than 2. This suggests that these alternative activity measures contain useful information not included in lags of the unemployment rate or past inflation Another difference about forecastability is that more than two variables outperform the unemployment rate uniformly across series. This is the case especially in the first differences specification. All variables except LPLNG outperform the unemployment rate.

Finally, in our case, specifications using the first difference of the activity variables and specifications using `gaps' produce similar forecasts by comparing RMSE. For the CPI series, first difference specification produces a slightly larger RMSE but for PCE series, the gaps specification produces a slightly larger RMSE. However, such differences are quite small. 

Based on the results in Table 1, we get the same conclusion as that paper that forecasts can be improved upon using a generalized Phillips curve based on measures of real aggregate activity other than unemployment. However, we in our simulation exercise we detected that the residuals from the models estimated for 1990:1-2013:12 are characterized by high level of autocorrelation, which violates the assumption of that the errors are independent. 

\begin{thebibliography}{99}
\bibitem [1]{stock} 
James H. Stock, Mark  W. Watson, \textit{Forecasting inflation}, Journal of Monetary Economics 44 (1999) 293-335


\section{Appendix}


\begin{table}[!ht]
\begin{flushleft}
\caption{Forecasting performance of alternative real activity measures}
\label{Table 1}
\begin{tabular}{ l  c c c c c }

\hline
\multicolumn{2}{ c }{Forecasting period: 2000-2013} &\multicolumn{2}{ c }{\textbf{PUNEW (CPI)}} & \multicolumn{2}{ c }{\textbf{GMDC (PCE)}}\\ 
\cline{1-6}
\textbf{Variable} & \textbf{Trans} & \textbf{Rel. MSE} & \textbf{$\lambda$} & \textbf{Rel. MSE} & \textbf{$\lambda$} \\
\hline
 Univariate &  &  0.53 & 1.02 (0.05) & 0.14 & 1.03 (0.05)\\ 
GARCH(1,1) &  &  &  & 0.11 & 0.91 (0.03)\\
 \multicolumn{2}{ c }{ \textit{Gaps specification}} &\multicolumn{2}{ c }{\textbf{}} & \multicolumn{2}{ c }{\textbf{}}\\ 
ip  & DT  & 0.94 & 0.77 (0.41)  & 0.79 & 0.97 (0.20)\\ 
gmpyq  &  DT & 0.94 & 1.11 (0.55) & 0.80 & 1.25 (0.33) \\ 
 msmtq & DT & 0.53 & 0.91 (0.19) & 0.71 & 0.81 (0.14)\\ 
 lplng & DT & 0.77 & 1.21 (0.38) & 0.74 & 1.00 (0.28)\\ 
 ipxmca & LV & 1.02 & 0.11 (0.73) & 0.75 & 0.96 (0.20) \\ 
 hsbp & LN & 1.34 & -0.16 (0.37) & 1.38 & 0.09 (0.31)\\
\multicolumn{2}{ c }{ \textit{First differences specification}} &\multicolumn{2}{ c }{\textbf{}} & \multicolumn{2}{ c }{\textbf{}}\\ 
ip & DLN & 0.92 & 1.24 (0.61)  & 0.76 & 1.43 (0.36)\\ 
gmpyq  & DLN &   0.95 & 1.33 (0.73) & 0.76 & 1.49 (0.36) \\ 
 msmtq & DLN & 0.92 & 2.27 (0.70) & 0.71 & 1.48 (0.37)\\ 
 lplng & DLN & 1.06 & -0.13 (0.94) & 0.79 & 1.80 (0.53)\\ 
 ipxmca & DLV & 0.89 & 1.64 (0.51) & 0.74 & 1.38 (0.35) \\ 
 hsbp & DLN & 0.95 & 1.55 (0.85) & 0.76 & 1.46 (0.35)\\
 dlhur & DLV & 0.96 & 1.24 (0.90) & 0.75 & 1.61 (0.44) \\
 \hline
\end{tabular}\\
\small{In parentheses: HAC robust standard errors (estimated using a Barlett kernel with 12 lags) as used in the original paper.\\
\textit{Note}: For a series $y_t$ , the transformations $x_t = f (y_t )$ are: $x_t = y_t$ (LV), $x_t = \Delta y_t$ (DLV),  $x_t = \Delta^2 y_t$(DDLV), $x_t = ln(y_t )$ (LN), $x_t=\Delta[ln(y_t)]$ (DLN),$x_t=\Delta^2[ln(y_t)]$ (DDLN), $x_t=ln(y_t )-\tau_t$ (DT) where $\tau_t$ is the HP-trend $y_t$}
\end{flushleft}
\end{table}


\begin{table}[!ht]
\caption{Relative mean-squared errors obtained from simulation Monte Carlo procedure (1000 repetitions)}
\label{Table2}
\begin{centering}
\begin{tabular}{ l  c c c c}
\hline
 & Min & Mean & Max & Std. dev.\\ 
\cline{1-3}
\hline
PUNEW & 3.31 & 4.07 & 7.64 &  0.59\\ 
GMDC & 4.76 & 7.07 & 11.96 &  0.88 \\ 
\end{tabular}
\end{centering}
\end{table}
 

%\begin{figure}[ht!]
%\centering
%\includegraphics[width=120mm]{punew_gmdc_all.eps}
%\caption{Annual inflation 1970-2013 \label{inflall}}
%\end{figure}


%begin{figure}[ht!]
%\centering
%\includegraphics[width=120mm]{punew_gmdc_2002.eps}
%\caption{Annual inflation 2002-2013 \label{infl2002}}
%\end{figure}








\end{thebibliography}


%----------------------------------------------------------------------------------------
%\end{multicols}
\end{document}

