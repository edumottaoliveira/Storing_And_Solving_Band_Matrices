\documentclass[a4paper,11pt]{article}
\usepackage{sobrapo-template}
\usepackage[brazil]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{array}
\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{multirow}
\usepackage{indentfirst}
\usepackage[portuguese,ruled,lined, linesnumbered]{algorithm2e}

\usepackage{tikz}
\usetikzlibrary{positioning}
\tikzset{
node of list/.style = { 
             draw, 
             fill=gray!20, 
             minimum height=6mm, 
             minimum width=6mm,
             node distance=6mm
   },
link/.style = {
     -stealth,
     shorten >=1pt
     },
array element/.style = {
    draw, fill=white,
    minimum width = 6mm,
    minimum height = 10mm
  }
}

\def\LinkedList#1{%
  \foreach \element in \list {
     \node[node of list, right = of aux, name=\element] {\element};
     \draw[link] (aux) -- (\element);
     \coordinate (aux) at (\element.east);
  } 
}




\title{RESOLUÇÃO DE SISTEMAS LINEARES COM MATRIZES ESPARSAS DE BANDA}

\begin{document}

\maketitle

\author{
\name{Eduardo Motta de Oliveira}
\institute{Universidade Federal do Espírito Santo}
\iaddress{Av. Fernando Ferrari, 514, Goiabeiras | Vitória - ES}
\email{edumottaoliveira@gmail.com}
}

\vspace{8mm}


\begin{resumo}
Este relatório foi desenvolvido para o primeiro exercício computacional do curso de Algorithmos Numéricos II. O exercício consiste em implementar dois modelos de armazenamento de matrizes de banda e resolve-los de forma otimizada usando os métodos diretos de resolução de sistemas lineares. O objetivo deste documento é mostrar o que foi implementado no código em anexo, explicar o motivo das decisões tomadas para armazenar e solucionar os sistemas com matrizes esparsas de banda e comparar os resultados para identificar a melhor solução ou os melhores métodos adotados em termos de perfomance computacional.

\end{resumo}



 
\newpage
\section{Introdução}
 
Matrizes de banda são uma categoria especial de matriz esparsa, onde os elementos não nulos distriuem-se numa faixa (ou uma banda) que cobre e envolve a diagonal principal. Por possuir elementos aglutinados numa área específica da matriz, as matrizes de banda podem fazer uso de estratégias mais otimizadas para o armazenamento dos seus elementos e resolução de listemas lineares.

O presente exercício define duas formas de armazenar esse tipo de matriz, que receberam a seguinte denominação:

\begin{itemize}
\item \textit{Matriz de Banda com Array de Listas}: Armazena listas das células não nulas por linha da matriz de forma indexada em um array principal.

\item \textit{Matriz de Banda com Compressão por Colunas}: Armazena uma matriz tradicional contendo as linhas não nulas da matriz de banda. Algumas células com valor nulo podem ser adicionadas para manter a distribuição dos índicies padronizada.
\end{itemize}

Os métodos de armazenamento serão exemplificados e melhor descritos nas duas seções seguintes. As últimas seções deste relatório serão dedicadas à análize dos metodos de rezolução usando os dois modelos de armazenamento.

\vspace{4mm}
\section{Matriz de Banda com Array de Listas}

O formato mais otimizado de armazenamento de uma matriz esparsa deveria eliminar toda célula nula para evitar o desperdício de memória. Como a quantidade de células nulas é variável nas linhas e colunas, o uso de listas encadiadas pode ser uma solução flexível. 

Porém, para a indexação dos elementos por linha e coluna novos valores terão de ser armazenados nas estruturas, reduzindo a eficiência do armazenamento. Desta forma, optou-se aqui por uma solução mista: linhas são armazenadas em listas encadeadas e, cada uma dessas listas é armazenada de maneira ordenada em um array, com acesso direto aos seus campos.

Um exemplo da transformação de uma matriz tradicional de banda em  uma na forma de array de listas é mostrado a seguir:

\[
   M=
  \left[ {\begin{array}{ccccc}
   a & b & . & . & . \\
   c & d & e & . & . \\
   . & f & g & h & . \\
   . & . & i & j & k \\
   . & . & . & l & m \\
  \end{array} } \right]
\]


\begin{center}
$M' =$
\begin{tikzpicture}
\foreach \index/\list in {0/{a,b,null}, 1/{c,d,e,null}, 2/{f,g,h,null}, 3/{i,j,k,null}, 4/{l,m,null}} {
   \node[array element] (aux) at (0,-\index) {\index};
   \LinkedList{\list}
}
\end{tikzpicture}
\end{center}


\vspace{4mm}
\section{Matriz de Banda com Compressão por Colunas}

O formato de armazenamento Matriz de Banda com Compressão por Colunas remove a maioria das células nulas enquanto justapõe os valores não nulos formando uma matrix com o mesmo número de linhas mas um menor número de colunas. 

Essa transformação pode ser exemplificada abaixo, onde a matriz $M$ é convertida na matriz $M'$ para que seja armazenada com mais eficiência. As células nulas, com valor igual a zero, foram substítuidas por pontos ($.$) na ilustração para facilitar a visualização. 

\[
   M=
  \left[ {\begin{array}{ccccc}
   a & b & . & . & . \\
   c & d & e & . & . \\
   . & f & g & h & . \\
   . & . & i & j & k \\
   . & . & . & l & m \\
  \end{array} } \right]
\]

\[
   M'=
  \left[ {\begin{array}{ccc}
   . & a & b \\
   c & d & e \\
   f & g & h \\
   i & j & k \\
   l & m & . \\
  \end{array} } \right]
\]

É possível observar nesse exemplo que a matriz $M (5 \times 5)$ foi transformada numa $M' (5 \times 3)$, ou seja, uma redução de 40\% no espaço de armazenamento necessário para representá-la. Essa redução do espaço nessessário irá depender da proporção de valores nulos da matriz original, que por sua vez também pode ser determinado pelo tamanho total da matriz e tamanho da banda.

Denominando $m$ e $n$ o número de linhas e colunas, reepectivamente, e $b$ o tamanho da banda (no exemplo acima era 3), então podemos chegar a conclusão que uma matriz $(m \times n)$ poderá ser armazenada por esse formato numa matriz $m \times b$, e a redução se dará numa proporção de $n - b$. 

A conversão de $M$ em $M'$, apesar de simples, apresenta um problema: como acessar os elementos da maneira correta para resolver um sistema linear onde a matriz pricipal está armazenada nesse formato? A seção 5 irá apresentar como esse processo será feito na resolução de um sistema pelo método de eliminação de Gauss.


\section{Solution metodology}

Firstly, the algorithm must generate an incomplete initial solution, in which there is still no time assignment at each node of the route, so a programming heuristic adapted from Mauri and Lorena (2009) define these values. With all attributes of the solution defined, an evaluation function is performed, which calculates the solution cost taking into account the attributes weights that negatively impact the quality of the result (eg waiting time, total distance travelled, total travel time Customer) and penalties for attributes that violate basic feasibility requirements (eg excess vehicle capacity, total times that violate time windows).

The ILS algorithm receive the initial solution created as input and performs local searches followed by perturbations to prevent the search from stagnate in local minima. The stopping criterion is based on the number of iterations without improvement. The return of the ILS algorithm is the best solution found in your search space.
It is interesting to note that the use of metaheuristics makes solving the problem arduous, but allows invalid solutions to be obtained. Solutions with a small degree of unfeasibility can be accepted in some real cases where larger problems are not solved by exact methods (Cordeau, 2006).

%\newline
\subsection{Initial solution}

The method applied here to build an incomplete initial solution considers the list of transport requests included as input. In this list, each request is formed by the pair boarding and disembarking, which must always be manipulated respecting its order of precedence. Initially, it creates for each vehicle a route containing only its garage of origin and destination, which in our case will always be the same garage because of the single garage model adopted. Thereafter, the boarding and disembarking nodes of each request are assigned and inserted in the routes respecting some specific method of insertion. Finally, in order to obtain a complete initial solution, a scheduling heuristic is performed, establishing the times of each node for the completion of the routes.

In this work, two models of insertion of requests were tested:

\begin{itemize}

\item Random Insertion: Random choices are made for a route and two consecutive positions within that route and then the boarding and landing nodes are inserted in the respective spaces.

\item Criteria Insertion: The list of requests is sorted chronologically by the average time of the time window. Then, the first request for boarding from list of requests is inserted at the end of the route whose last vertex is closest in terms of Euclidean distance. At the end, the landing node associated with the current customer request is inserted in the sequence, providing the least travel time possible for that customer.
The Criterion Insertion creates an initial solution with a drastic reduction in the number of violations, if compared to the random method, which facilitates convergence for the best solution and tends to optimize the search process.

\end{itemize}

It can be observed that invalid solutions can be obtained, however any infeasibility is penalized in the objective function and, therefore, it tends to disappear along the iterations of the ILS algorithm.

%\newline
\subsubsection{Programming heuristic}
The solution to the problem is not only limited to determining the order of customers served and their distribution in the vehicles. The final solution should also establish time intervals for arrival, departure, start and end of service at each node, in addition to the occupation of the vehicles at each node.

Thus, a programming heuristic is required to compute these values for the initial solution and to recompute those values to each new solution generated in the search process. The calculation of these times for each route is based on the mathematical model presented by Mauri and Lorena (2009), where, for each attended node  $i\ (\forall i \in N)$ can be defined:

\begin{itemize}
\item Vehicle arrival time:  $A_i = 0$ if $i \in \{0\}$ and $A_i = D_{i-1} + t_{i-1,i}$ if $i \in \{P  \cup D \cup \{0\}\}$;
\item Vehicle start time: $D_i = 0$ if $i \in \{2n+1\}$ and $D_i = B_i + d_i$ if $i \in \{P \cup D\}$ e $D_i=B_i$ se $i \in \{2n+1\}$;
\item Service start time: $B_i = D_i$ if $i \in \{0\}$ and $B_i = max\{e_i,A_i \}$ if $i \in \{P \cup D \cup\ \{2n + 1\}\}$;
\item Waiting time before start of service: $W_i = 0$ if $i \in \{0\}$ and $W_i = B_i - A_i$ if $i \in \{P \cup D \cup \{2n + 1\} \}$;
\item Load (number of seats occupied) after the end of service: $Q_i = 0$ if $i \in \{0, 2n + 1\}$ and $Q_i = Q_{i-1} + q_i$ if $i \in \{P \cup D\}$;
\item Customer travel time is $L_i = B_{n+i} - D_i$.
\end{itemize}

It is important to note that the programming look for to anticipate the service on the nodes as much as possible ($B_i = max\{e_i,A_i \}$) by moving the service start time on each node to the closest point of the begining of its time window. The justification lies in the fact that this provides a time slack which may be necessary for a better arrangement of service times in the following nodes.

However, anticipating the start of services can create long waiting times between calls (atendimentos), which would be detrimental to the cost of the solution. For this reason, after initial programming it is necessary to try to delay the start of the service at the initial nodes of the route in order to minimize this wait. The concept of Forward Time Slack, initially proposed by Savelsbergh (1992), was then employed to define the maximum possible delay of the service start time at each node so as not to violate its time window constraints. Thus, customer service is delayed gradually from the start of the route without creating new violations.

%\newline
\subsection{Objective function}

The objective function was presented in section \ref{formulacao} and provides a means to evaluate the quality of each new solution generated by ILS.

It is important to emphasize that the full cost evaluation of the solutions, other than in the initial solution, should be avoided. As the generated and analyzed neighbor solutions differ in a few respects, it makes sense to only perform the evaluation function on the routes that have undergone modifications and thus reduce the required number of processing cycles in each iteration of the search process.

%\newline
%Tempo entre os vertices e calculado com base na distancia euclidiana!!!!
\subsection{Neighborhood structure}

The search for a better solution in DARP involves testing neighboring solutions. Mauri and Lorena (2009) use three movements to generate neighborhoods: reordering, reallocation and node exchange between routes. According to them, these movements are based on others often found in DARP works (Cordeau and Laporte, 2003; Jorgensen, 2007; Savelbergh, 1992).


The \textit{reordering} movement consists of replacing a node of any route in another position of the same route without violating the relation of precedence between loading and unloading.

The \textit{exchange} movement is applied to the nodes of two clients served by two different routes. Routes and customers are randomly selected and their pairs of nodes are swapped out causing customers to change position and route.

The \textit{relocation} move is performed on any two routes by removing the loading and unloading nodes of some client from one of the routes and reinserting them at random locations on the other route.

These movements can generate invalid solutions. In this case, these solutions are penalized in the objective function.


\begin{figure}[!htbp]
  \centering
%  \subfigure[ref1][]{\includegraphics[height=4.5cm]{diagrama_1_en.png}}
%  \subfigure[ref2][]{\includegraphics[height=4.5cm]{diagrama_2_en.png}}
%  \subfigure[ref3][]{\includegraphics[height=4.5cm]{diagrama_3_en.png}}
%	\includegraphics[height=4.5cm]{diagrama_2.png} 
%	\includegraphics[height=4.5cm]{diagrama_3.png}
  \caption{Examples of movements. (a) reordering. (b) exchange. (c) relocation.}
%    \label{figmodelo1}
  
\end{figure}

\subsection{Local Search}

Also known as a hill clibing, Local Search proposes to find the local optimum generating, at each iteration, neighboring solutions based on the best current solution and accepting only better solutions than the current one or those that meet a certain criterion of acceptance. At the end of the process, the best solution found is returned.

In this article, two versions of Local Search were implemented, which differ in the method of neighborhood generation.The first version implements neighbourhood generation in a totally random fashion: at each iteration, a movement is randomly selected and applied to a set of routes, positions, and customers that are also randomly selected. The second version, however, allows a comprehensive analysis of the neighborhood: from random choices of movement, customers, and routes, all possible customer movements on the routes are generated, seeking to cover a larger area of the neighborhood.

The two local search functions are terminated when a predetermined number of iterations without improving the quality of the solution is detected.

%\newline
\subsection{Busca Local Iterada}

Iterated Local Search (ILS), is a simple and efficient meta-heuristic. From an initial solution it generates a local optimum and, at each iteration, performs a perturbation followed by a local search on the disturbed solution, producing a new local optimum. The perturbations are functions that alter the solution in order to generate neighboring solutions that are not so close that they can fall into the same local optimum or as far away as they can behave as random, generating a traditional local search from a new initial random solution. In practice, the ILS algorithm consists of a sequence of local queries interspersed by perturbations. Its pseudo-code is given by Algorithm 1. \\

%Código
%\resizebox{12cm}{!} {
 \begin{algorithm}[H]
 \footnotesize {
 \DontPrintSemicolon
%   \SetAlgoLined
   \Entrada {$s_0$ \tcc*[r]{Initial solution} }
   \Saida {$s$ \tcc*[r]{Best solution found} }
   %\Require
   %\Ensure
   \Begin{
% \SetKwProg{myproc}{Procedure}{}{}
	$LocalSearch(s_0)$\\
	$s \gets s_0$ \\
	$cost\gets costFunction(s)$\\
\tcc*[l]{Hold loop if the maximum number of iterations without improvement has been exceeded}
		\While{numIter $<$ maxNumIter} {
	   		$s*\gets s$\\
  		$pertubation(s*)$\\
  		$LocalSearch(s*)$\\
  		$cost*\gets CostFunction(s*)$\\  
  		$numIter\gets numIter + 1$\\ 
  		\If{cost* $<=$ cost} {
  			$s\gets s*$\\
  			$numIter\gets 0$\\
  		}
	   }
	   \textbf{return} $s$
%	
%   \Retorna{$\sigma(S)$}
%   \label{alg1}
   \caption{\textsc{Iterated Local Search}} }
   }
 \end{algorithm}

%}

The stopping criterion is defined by the number of iterations without improvement. When a large sequence of iterations fails to reduce the cost of the solution, the algorithm stops and returns the best solution obtained during execution.

Line 7 calls a perturbation function that performs a different movement from those used in neighborhood generation. Similar to a 2-opt movement, a node is randomly chosen and swapped with the next attendance node within the route, creating a small but distinct disturbance that tends to be more difficult to reverse during local search and therefore tends to direct the search for alternative regions. The necessary precautions for the effectiveness of this movement are the prohibition of permutations that disregard the relations of precedence between embarkation and disembarkation and the application of a level of perturbation proportional to the size of the solution, which is obtained with its application in quantity proportional to the number of customers or vehicles of the problem.

%\newline
\section{Computational results}

For the experiments the instances of Cordeau and Laporte (2003) were used. These are diverse and present situations with up to 13 vehicles and 144 clients and are therefore widely used as a basis for comparison for several studies in the literature on DARP.

The computational tests were run on an Intel Core i5-2400 computer with 4GB of RAM. All code was produced in C language and the Ubuntu 14.04 64 bit system was used for compilation and execution.

In order to make a comparison with the work of Mauri and Lorena (2009) the weights in the objective function were defined in the same way as suggested by these authors (ie each of the essential requirements was defined the weight of 1500 and for the non essential requirements the weights were distributed as follows: weight 8 for the total distance travelled , weight 1 for the total duration of the routes and for the total waiting time and weight 3 for the total travel time of the customers).

The stopping condition of the ILS algorithm was defined by the detection of 15 iterations without improvement. 

Four variations of the main algorithm were implemented, each of which was performed 15 times for each instance. Table 1 contains the costs of the solutions obtained and the description of the results in each column, where:

\begin{itemize}
\item M\&L: Represents the best results obtained by Mauri and Lorena (2009), which use Simulated Annealing in conjunction with distribution and programming techniques similar to this one;
\item A: Corresponds to the execution of the algorithm with the generation of the initial solution by criterious insertion and the local search with random neighbourhood;
\item B: Corresponds to the execution of the algorithm with random initial solution and the local search with random ;
\item C: Corresponds to the execution of the algorithm with the generation of the initial solution by careful insertion and the local search with comprehensive analysis of the neighbourhood;
\item D: Corresponds to the execution of the algorithm with random initial solution and the local search with comprehensive neighbourhood analysis.
\end{itemize}


\begin{table}[h]
%\label{tab1}
\centering 
\resizebox{13cm}{!} {
\begin{tabular}{|c|c|c|c|c|c|c|c|}

%\multicolumn{1}{c|}{} & \multicolumn{5}{c|}{\textbf{Custo total}} \\ 
\hline
\multirow{2}{*}{\textbf{Instance}}& \multirow{2}{1,6cm}{\centering\footnotesize{\textbf{Number of vehicles}}} & \multirow{2}{1,6cm}{\centering \footnotesize{\textbf{Number of clients}} } & \multicolumn{5}{c|}{\textbf{Best total cost obtained}} \\ 
 \cline{4-8}
 & & & M\&L & A & B & C & D \\ \hline
pr01 & 3 & 24 & \textbf{3.677,91} & 3.683,06 & 3.683,06 & 3.683,06 & 3.683,06 \\ \hline
pr02 & 5 & 48 & 7.017,34 & \textbf{6.736,14} & 7.026,23 & 6.848,75 & 6.764,55 \\ \hline
pr03 & 7 & 72 & 11.873,76 & \textbf{11.777,33} & 11.872,40 & 11.782,09 & 11.848,83 \\ \hline
pr04 & 9 & 96 & 13.725,92 & 13.440,90 & \textbf{13.361,57} & 13.443,61 & 13.506,28\\ \hline
pr05 & 11 & 120 & 15.736,66 & 15.424,75 & \textbf{15.355,88} & 15.842,20 & 15.735,05 \\ \hline
pr06 & 13 & 144 & 20.465,39 & 19.755,09 & \textbf{19.732,66} & 20.215,30 & 20.200,94 \\ \hline
pr07 & 4 & 36 & 5.610,05 & 5.538,53 & \textbf{5.535,19} & 5.555,80 & 5.557,23 \\ \hline
pr08 & 6 & 72 & 11.343,19 & 11.178,02 & 11.233,52 & \textbf{10.951,26} & 11.010,31 \\ \hline
pr09 & 8 & 108 & \textbf{15.632,09} & 16.360,27 & 17.023,64 & 17.272,91 & 16.770,04 \\ \hline
pr10 & 10 & 144 & 22.430,00 & \textbf{21.422,83} & 21.429,14 & 21.989,03 & 22.140,44 \\ \hline
pr11 & 3 & 24 & 3.379,74 & \textbf{3.355,78} & 3.339,69 & 3.356,98 & \textbf{3.355,78} \\ \hline
pr12 & 5 & 48 & 5.889,56 & 5\textbf{.770,95} & 5.774,73 & 5.810,89 & 5.772,90 \\ \hline
pr13 & 7 & 72 & 11.006,12 & 10.821,40 & \textbf{10.757,97} & 10.878,19 & 10.831,36 \\ \hline
pr14 & 9 & 96 & 12.807,87 & \textbf{12.482,34} & 12.552,40 & 12.650,70 & 12.640,24 \\ \hline
pr15 & 11 & 120 & 14.544,13 & 14.207,21 & \textbf{14.064,74} & 14.488,02 & 14.285,25 \\ \hline
pr16 & 13 & 144 & 18.518,82 & 18.291,03 & \textbf{18.037,87} & 18.393,58 & 18.506,09\\ \hline
pr17 & 4 & 36 & 5.136,37 & 5.150,72 & 5.159,55 & 5.077,09 & \textbf{5.074,72} \\ \hline
pr18 & 6 & 72 & 10.703,17 & 10.406,32 & \textbf{10.365,7}9 & 10.382,66 & 10.447,71 \\ \hline
pr19 & 8 & 108 & 15.013,71 & 14.786,70 & \textbf{14.727,11} & 14.947,73 & 20.481,29 \\ \hline
pr20 & 10 & 144 & 1\textbf{9.969,15} & 19.618,59 & 20.255,43 & 20.356,89 & 20.204,22 \\ \hline
\hline 
\textbf{Total:} & - & - & 244.480,95 & \textbf{240.207,96} & 241.288,57 & 247.334,31 & 243.165,83 \\ \hline

\end{tabular}
}
\caption{Results from the execution of the algorithms of Mauri and Lorena (2009) and the four variants of the ILS algorithm for the 20 instances of Cordeau and Laporte (2003). Best values are shown in bold.}
\end{table}

Observing the values in bold in Table 1 we can consider that the second variant of the ILS algorithm (B) produced the greatest number of best solutions. It is also possible to identify that the total in the last line favors the first variant (A), with a small cost reduction of 244,480.95 (given by M\&L) to 240,207.96.

The last two variants (C and D) also obtained advantageous results regarding the results of the work of Mauri and Lorena (2009), which was already able to overcome the results of Cordeau and Laporte (2003) and Jorgensen \textit{et al.} (2007). Nevertheless, they presented inferior results, in general terms, to the first two variants.

Run times ranged from 0.49 seconds (pr01, variant C) to 108.02 seconds (pr16, variant B). The average execution times of the set of instances for each variant were, in seconds: 30.60 (A), 32.06 (B), 16.73 (C) and 17.55 (D). The execution times in the work of Mauri and Lorena (2009) for the same instances can not be compared directly with the times of the ILS algorithm because they were obtained in a different hardware.

\begin{table}[h]
\centering 
\resizebox{15cm}{!} {
\begin{tabular}{|c|cc|cc|cc|cc|cc|}
\hline
\multirow{2}{*}{\textbf{Instance}} & 
\multicolumn{2}{c|}{\textbf{Total cost}} &
\multicolumn{2}{c|}{\textbf{Distance}} & 
\multicolumn{2}{c|}{\textbf{Duration}} & 
\multicolumn{2}{c|}{\textbf{Waiting time}} & 
\multicolumn{2}{c|}{\textbf{Travel time}} \\
 \cline{2-11} 
 %\rowcolor{lightgray}
& M\&L & B & M\&L & B & M\&L & B & M\&L & B & M\&L & B\\
\hline
pr01 & \textbf{3.677,91} & 3.683,06 & 252,79 & 273,70 & 831,30  & 863,65  & 98,51  & 109,95 & 241,93 & 173,29 \\ \hline
pr02 & \textbf{7.017,34 }& 7.026,23 & 437,45 & 435,28 & 1.992,34 & 1.961,60 & 594,90 & 562,67 & 310,17 & 330,19 \\ \hline
pr03 & 11.873,76 & \textbf{11.872,40} & 831,74 & 882,22 & 2.404,67 & 2.454,46 & 132,93 & 183,53 & 894,08 & 862,32 \\ \hline
pr05 & 15.736,66 & \textbf{15.355,88} & 1.085,45 & 1.107,87  & 3.920,25 & 3.655,17 & 434,81 & 187,22 & 899,35 & 989,96 \\ \hline
pr09 & \textbf{15.632,09} & 17.023,64 & 1.064,23 & 1.095,94  & 3.258,66 & 3.193,07 & 34,42  & 7,17 & 1.275,06 & 1.872,07 \\ \hline
pr10 & 22.430,00 & \textbf{21.429,14} & 1.392,09 & 1.449,50  & 4.475,42 & 4.484,12 & 203,33 & 164,20 & 2.204,85 & 1.753,81 \\ \hline
pr11 & 3.379,74 & \textbf{3.339,69} & 251,85 & 267,99 & 738,42  & 742,17  & 6,57 & 5,26 & 206,66 & 178,97 \\ \hline
pr12 & 5.889,56 & \textbf{5.774,73} & 436,69 & 433,22 & 1.428,44 & 1.393,45 & 31,75 & 3,88 & 311,95 & 313,61 \\ \hline
pr15 & 14.544,13 & \textbf{14.064,74} & 1.010,09 & 1.045,50  & 3.654,02 & 3.489,92 & 243,94 & 100,91 & 855,16 & 853,95 \\ \hline
pr16 & 18.518,82 & \textbf{18.037,87} & 1.289,31 & 1.319,29  & 4.318,33 & 4.180,19 & 149,02 & 38,17  & 1.245,66 & 1.241,11 \\ \hline
pr17 & \textbf{5.136,37} & 5.159,55 & 375,67 & 371,61 & 1.095,67 & 1.123,56 & 0,00 & 11,18  & 345,10 & 295,28 \\ \hline
pr19 & 15.013,71 & \textbf{14.727,11} & 1.041,09 & 1.064,02  & 3.315,28 & 3.238,89 & 114,19 & 49,83  & 1.085,18 & 1.068,64 \\ \hline
pr20 & \textbf{19.969,15} & 20.255,43 & 1.414,65 & 1.417,48  & 4.332,69 & 4.222,23 & 38,04  & 20,31  & 1.427,08 & 1.812,51 \\ \hline 
\hline
\textbf{Total:} & 244.480,95  & 241.288,57  & 10.883,10  & 11.163,62 & 35.765,49 & 35.002,48 & 2.082,41 & 1.444,28 & 11.302,23  & 11.745,71  \\ \hline 
\end{tabular}
}
\caption{Comparison of the results of Mauri and Lorena (2009) and Iterated Local Search with random initial solution and random neighbourhood structure.}
\end{table}

By adopting the second variant of the ILS algorithm (B) and comparing its results in detail with those of Mauri and Lorena in Table 2, it is perceivable that the method proposed here allowed the reduction of waiting times and travel times.

It can be seen from Table 2 that the total travel time has worsened from 11,302.23 to 11,745.71, but this amounts to a difference of only 3.92\%. It is also seen that the distance had a slight worsening of 2.57\%. In contrast, the total waiting time was reduced from 2,082.41 (M\&L) to 1,444.28, ie a reduction of 30.64\%, which also resulted in a reduction in the duration of the routes of 2.13\%.

The results of the pr04, pr06, pr07, pr08, pr13, pr14 and pr18 instances were not presented in the work of Mauri and Lorena and therefore could not be used as a basis for comparison.
However, in the analysis of the total costs, which consider all the essential and non-essential aspects of the problem, we noticed that the presented algorithm improved in 15 out of 20 instances and that there was a reduction in total costs of 244,480.95 (M\&L) for 241,288.57, ie a reduction of 1,3\%.
%\newline
\section{Conclusion}

In this article, Iterated Local Search was used to deal with the multiobjective dial-a-ride problem (DARP) in a simple and efficient way.

Variations of initial solution generation and neighbourhood generation methods were tested.
Different initial solution generation systems did not significantly interfere with the final results. Already the neighbourhood generation variants significantly affected the results.

The computational experiments showed a reduction of the costs of the majority of the instances, when compared with the results of Mauri and Lorena (2009), thanks to the great reduction of the waiting times of the vehicles, which also implied in the reduction of the duration of the routes. Thus, the comparative results demonstrate that the approach adopted and the use of Iterated Local Search form an extremely competitive alternative for the resolution of DARP.

Future work should focus on improving the efficiency of the presented algorithm, as well as allow the handling of more complex instances with multiple garages or involving extremely infeasible cases where not all users can be served without violating basic restrictions.

%\newline
%\section{Agradecimentos}
%À UFES e à Pró Reitoria de pesquisa e Pós Graduação pela bolsa de iniciação científica concedida para a realização dessa pesquisa (processo no xxxxx). 
%
%Ao orientador Dr. André Renato Sales Amaral pela oportunidade e confiança.
%
%Aos colegas do LABOTIM, principalmente à Ramoni Z. Sedano e Roger Senna, que contribuíram com sugestões e dicas para o sucesso desse trabalho.
%\newline
%\newline

%\section{Agradecimentos}

%Eduardo Motta de Oliveira agradece a bolsa de Iniciação Científica da UFES %2014/2015. Este trabalho teve o apoio financeiro da FAPES (processo nº %56145659/11).


\bigskip
\noindent{\bf References}

\noindent \textbf {Cordeau, J.-F.}, A Branch-and-Cut Algorithm for the Dial-a-Ride Problem, \textit{Operations Research}, v. 54, n. 3, p. 573-586, 2006.
	
\noindent \textbf {Coudeau, J.-F; Laporte, G.}, A tabu search heuristic for the static multi-vehicle dial-a-ride problem, \textit{Transportation Research Part B: Methodological}, v. 37, n. 6, p. 579-594, 2003.

\noindent \textbf {Coudeau, J.-F; Laporte, G.}, The dial-a-ride problem: variants, modeling issues and algorithms, \textit{Quarterly Journal of the Belgian, French and Italian Operations Research Societies}, n. 1, p. 89-101, 2003.

\noindent \textbf {Coudeau, J.-F; Laporte, G.}, The dial-a-ride problem:  models and algorithms, \textit{Annals of Operations Research}, v.153, n. 1, p. 29-46, 2007.

\noindent \textbf {Cubillos C.; Urra E.; Rodríguez N.},Application of Genetic Algorithms for the DARPTW Problem, \textit{Int. J. of Computers, Communications and Control}, v. 4, n. 2, 127-136, 2009.

\noindent \textbf {Deleplanque, S.; Derutim, J.-P; Quilliot, A.}, Anticipation in the Dial-a-Ride Problem: an introduction to the robustness, \textit{Proceedings of the 2013 Federated Conference on Computer Science and Information Systems}, p. 299-305, 2013.

\noindent \textbf {Jorgensen, R. M.; Larsen, J.-P; Bergvinsdottir, K. B}, Solving the dial-a-ride problem using genetic algorithms, \textit{Journal of the Operational Research Society}, v. 58, n. 10, p. 1321-1331, 2009.

\noindent \textbf {Mauri G. R.; Lorena L. A. N.}, Uma nova abordagem para o problema dial-a-ride, \textit{Produção}, v. 19, n. 1, 41-54, 2009.

\noindent \textbf {Savelsbergh, M. W. P.}, The vehicle routing problem with time windows: minimizing route
duration, \textit{ORSA Journal on Computing}, v. 4, n. 2, p. 146-154, 1992.

\noindent \textbf {Soldano, A.; Valandro, F.}, Online and offline algorithms for the Dial-a-Ride problem: design and implementation of operational research heuristic algorithms for transportation problems, Politecnico di Milano, 2004.%100/100 cum laude in mathematics, physics and computer science - Politecnico di Milano


\end{document}


