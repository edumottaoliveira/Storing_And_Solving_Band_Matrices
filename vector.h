#ifndef VECTOR_H
#define VECTOR_H

typedef struct {
	double* cells;
	int numberOfElements;
} Vector;

Vector* allocateVector(int numberOfElements);

void freeVector(Vector* vector);

void printVector(Vector* vector, char* description);


#endif
