#include "bandMatrixALSystem.h"
#include <stdio.h>
#include "util.h"

BandMatrixALSystem* createBandMatrixALSystem(BandMatrixAL* matrixA, Vector* vectorB) {

	BandMatrixALSystem* ls = (BandMatrixALSystem*) malloc (sizeof(BandMatrixALSystem));
	ls->matrixA = matrixA;
	ls->vectorB= vectorB;
	Vector* sol = allocateVector(matrixA->numberOfColumnsExpanded);
	ls->vectorSolution = sol;
	return ls;
}

void freeBandMatrixALSystem(BandMatrixALSystem *ls) {
	if (ls == NULL) {
		printf("null");
		return;
	}
	freeBandMatrixAL(ls->matrixA);
	freeVector(ls->vectorB);
	freeVector(ls->vectorSolution);
	FREE(ls);
}

void printBandMatrixALSystem(BandMatrixALSystem *ls) {
	printBandMatrixAL(ls->matrixA,"A");
	printVector(ls->vectorB,"b");
	printVector(ls->vectorSolution,"Sol");
}

void solveBandMatrixALSystem(BandMatrixALSystem* linearSystem) {
		if (linearSystem == NULL)
			return;
		printf("\nResolvendo sistema linear...\n");
		performGausReductionBandMatrixAL(linearSystem);
		performBackSubstitutionBandMatrixAL(linearSystem);
}

void performGausReductionBandMatrixAL(BandMatrixALSystem* linearSystem) {
		BandMatrixAL* a = linearSystem->matrixA;
		double* b = linearSystem->vectorB->cells;
		int numRows = linearSystem->matrixA->numberOfRows;
		int numCols = linearSystem->matrixA->numberOfColumnsExpanded;
		int offset = linearSystem->matrixA->offsetFirstRow;
		double m, pivot;

		/* k: linha e coluna do pivo */
		for (int k = 0; k < numRows-1; k++) {
			pivot = getElemBandMatrixAL(a,k,k);
			if (pivot == 0.0)
				ERROR("Divisao por zero.");

			for (int i = k+1; i < k+1-offset && i < numCols; i++) {
				MatrixCell* firstCellRowI = getCellBandMatrixAL(a,i,k);
				m = firstCellRowI->value/pivot;
				firstCellRowI->value = 0.0;

				for (int j = k+1; j < numCols; j++) {
					MatrixCell* currentCellRowI = getCellBandMatrixAL(a,i,j);
					MatrixCell* CellPivotRow = getCellBandMatrixAL(a,k,j);

					if (CellPivotRow != NULL) {
						currentCellRowI->value = currentCellRowI->value - m * CellPivotRow->value;
					} else {
						break;
					}
				}
				b[i] = b[i] - m*b[k];
			}
		}
}

void performBackSubstitutionBandMatrixAL(BandMatrixALSystem* linearSystem) {

	BandMatrixAL* a = linearSystem->matrixA;
	double* b = linearSystem->vectorB->cells;
	double* sol = linearSystem->vectorSolution->cells;
	int numRows = linearSystem->matrixA->numberOfRows;
	int numCols = linearSystem->matrixA->numberOfColumnsExpanded;
	int offset = linearSystem->matrixA->offsetFirstRow;

	for (int i = numRows-1; i >= 0; i--) {
		double sum = 0.0;
		int offsetIteratedRow = offset + i;

		for (int j = i+1 ; j < numRows; j++) {
			MatrixCell* cellMatrix = getCellBandMatrixAL(a,i,j);
			if (cellMatrix == NULL)
				break;

			sum += cellMatrix->value * sol[j];
		}
		sol[i] = (b[i] - sum)/ getElemBandMatrixAL(a,i,i);
	}
}

