#ifndef MATRIX_H
#define MATRIX_H

typedef struct {
	double** cells;
	int numberOfRows;
	int numberOfColumns;
} Matrix;

Matrix* allocateMatrix(int numberOfRows, int numberOfColumns);

void freeMatrix(Matrix* matrix);

void printMatrix(Matrix* matrix, char* description);


#endif
